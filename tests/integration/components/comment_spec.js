import { shallowMount } from '@vue/test-utils';
import * as timeago from 'timeago.js';
import { GlTooltip } from '@gitlab/ui';
import { GlComment, GlMarkdown } from '~/components';
import { dateTimeMixin } from '~/utils/datetime';

jest.mock('~/utils/datetime');
jest.mock('timeago.js');

const formattedTimeAgo = '3 months ago';
const formattedDateTime = 'Feb 21, 2020 9:20am';
const formattedTimezone = 'GMT+2';
timeago.format.mockReturnValue(formattedTimeAgo);

dateTimeMixin.methods.formatDateTime.mockReturnValue(formattedDateTime);
dateTimeMixin.methods.formatDateTimeZone.mockReturnValue(formattedTimezone);

describe('Comment', () => {
  let wrapper;
  const comment = {
    created_at: '2020-02-21T07:05:38.261Z',
    note: 'Incident update comment',
  };

  function mountComponent() {
    wrapper = shallowMount(GlComment, {
      propsData: {
        note: comment.note,
        createdAt: comment.created_at,
      },
    });
  }

  beforeEach(() => {
    mountComponent();
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  const findText = () => wrapper.find(GlMarkdown);
  const findTooltip = () => wrapper.find(GlTooltip);

  it('renders the component', () => {
    expect(wrapper.element).toMatchSnapshot();
  });

  it('renders comment text', () => {
    expect(findText().exists()).toBe(true);
  });

  it('renders comment post date with tooltip', () => {
    expect(wrapper.element.textContent).toContain(`Posted ${formattedTimeAgo}`);
    expect(findTooltip().exists()).toBe(true);
    expect(findTooltip().text()).toContain(`${formattedDateTime} ${formattedTimezone}`);
  });
});
