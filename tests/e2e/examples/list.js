describe('List page loads successfully with data', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Loads the page markup correctly and shows incidents', () => {
    cy.get('[data-qa-selector="navbar"]')
      .find('.gl-link')
      .should('have.attr', 'href')
      .and('include', 'https://support.gitlab.com/hc/en-us/requests/new');

    cy.get('[data-qa-selector="incident-list"]')
      .find('[data-qa-selector="incident"]')
      .should('have.length', 3);
  });
});

describe('List Page loads successfully with no data', () => {
  beforeEach(() => {
    cy.server();
    cy.route({
      method: 'GET',
      url: '/data/list.json',
      response: [],
    });
    cy.visit('/');
  });

  it('Loads a alert with successful feedback for user', () => {
    cy.get('[data-qa-selector="incident-list-success-alert"]').contains(
      'There are no active incidents to report. All systems are operational.',
    );
  });
});

describe('List Page loads unsuccessfully', () => {
  beforeEach(() => {
    cy.server();
    cy.route({
      method: 'GET',
      url: '/data/list.json',
      response: [],
      status: 500,
    });
    cy.visit('/');
  });

  it('Loads a alert with warning feedback for user', () => {
    cy.get('[data-qa-selector="incident-list-error-alert"]').contains(
      'There was an error fetching the incidents. Please refresh the page to try again.',
    );
  });
});
