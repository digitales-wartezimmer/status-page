import Vue from 'vue';
import VueSanitize from 'vue-sanitize';
import App from '~/app.vue';
import router from '~/router';
import installGlEmojiElement from './behaviors/gl_emoji';

import '~/assets/styles/global.scss';

// https://github.com/apostrophecms/sanitize-html
const sanitizeOptions = {
  // whitelist allowed tags
  allowedTags: VueSanitize.defaults.allowedTags.concat([
    'h1',
    'h2',
    'img',
    'details',
    'kbd',
    'dd',
    'gl-emoji',
  ]),
  // allow all attributes
  allowedAttributes: false,
};
Vue.use(VueSanitize, sanitizeOptions);

Vue.config.productionTip = false;
installGlEmojiElement();

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
