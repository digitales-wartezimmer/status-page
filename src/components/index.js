/* eslint-disable import/prefer-default-export */
import GlListIncident from '~/components/list_incident.vue';
import GlHead from '~/components/header.vue';
import GlMarkdown from '~/components/markdown.vue';
import GlComment from '~/components/comment.vue';

export { GlListIncident, GlHead, GlMarkdown, GlComment };
