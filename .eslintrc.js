const isProdOrCI = () => ["production", "CI"].includes(process.env.NODE_ENV);
const vueConfig = require('./vue.config');

module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["@vue/prettier", "plugin:@gitlab/default"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": isProdOrCI() ? "error" : "off",
    "no-debugger": isProdOrCI() ? "error" : "off",
    "prettier/prettier": "error"
  },
  settings: {
    'import/resolver': {
      node: vueConfig.configureWebpack,
      webpack: {
        config: require.resolve('@vue/cli-service/webpack.config.js')
      },
    },
  },
    overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/integration/**/*_spec.{j,t}s?(x)"
      ],
      env: {
        jest: true
      }
    }
  ]
};
